# Fedora install helper scripts

Helper scripts I use to simplify my life when customizing my [Fedora Everything](https://alt.fedoraproject.org/) installation.

After having set up basic packages such as `gnome-shell`, run `prepare-repository.sh` in order to set up repositories, removing unwanted default repos, adding RPMFusion free repository and finally upgrade your system. After that script a system reboot is advised.

Run `preinstall` to add some basic software, needed before running `configs.sh`. The script `config.sh` will also create proper soft links for my own dotfiles (the script is crafted for my personal use case).

After that, install desired collections of software by passing it to `dnf`. For instance, to install `gnome-essentials` and `sway` collections, run

```
sudo dnf install $(cat gnome-essentials sway)
```
