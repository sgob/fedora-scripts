# Hide your firefox tabs like a pro

1. Find your profile folder (hence referred to as ${PROFILE}): go to about:support and look at the line that says "Profile folder".
2. Toggle the relevant about:config flags.

```
toolkit.legacyUserProfileCustomizations.stylesheets
layers.acceleration.force-enabled
gfx.webrender.all
gfx.webrender.enabled
layout.css.backdrop-filter.enabled
svg.context-properties.content.enabled
```

3. Close Firefox.
4. Put this in ${PROFILE}/chrome/userChrome.css (create the file if it doesn't already exist):

```
#TabsToolbar
{
    visibility: collapse;
}
```

5. Start up Firefox again.
