#!/usr/bin/env bash
#set -e
# -----------------------------------------------------------------------------
# FEDORA CONFIGS FILE
# -----------------------------------------------------------------------------

sudo tee -a /etc/dnf/dnf.conf > /dev/null <<EOF
max_parallel_downloads=10
EOF
echo ""
echo "Editing dnf.conf complete."
echo ""
#echo "Installing oh-my-zsh..."
#sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
#mv .zshrc .zshrc.bak
#echo "Installation of oh-my-zsh completed."
echo ""
echo "Cloning dotfiles..."
cd ~
git config --global init.defaultBranch main
git init
echo '*' > .gitignore
git remote add origin git@git.sr.ht:~sgob/dotfiles
git fetch
git checkout -f main
echo "Cloning dotfiles... done"
echo ""
echo "Applying GNOME Shell improvements..."
gsettings set org.gnome.desktop.interface font-antialiasing 'rgba'
gsettings set org.gnome.desktop.sound allow-volume-above-100-percent true
gsettings set org.gnome.settings-daemon.plugins.color night-light-enabled true
gsettings set org.gnome.desktop.interface monospace-font-name "Monospace 12"
echo ""
echo "Improvements applied."
echo "Configuration complete."
