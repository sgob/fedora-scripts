# Installs RPMFusion free
sudo dnf install -y https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm 

# Install RPMFusion non-free
# sudo dnf install -y https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm 

# No need for this repository since we are adopting RPMFusion
sudo sed -i 's/enabled=1/enabled=0/g' /etc/yum.repos.d/fedora-cisco-openh264.repo


# Eventually disables testing repositories
sudo sed -i 's/enabled=1/enabled=0/g' /etc/yum.repos.d/fedora-updates-testing-modular.repo
sudo sed -i 's/enabled=1/enabled=0/g' /etc/yum.repos.d/rpmfusion-free-updates-testing.repo

# sudo wget https://fedorapeople.org/groups/virt/virtio-win/virtio-win.repo -O /etc/yum.repos.d/virtio-win.repo

# sudo dnf install virtio-win -y && sudo dnf --enablerepo=virtio-win-latest upgrade virtio-win -y 

# Disables machine counting 
sudo sed -i 's/countme=1/countme=0/g' /etc/yum.repos.d/* 

# Complete upgrade of the system
sudo dnf upgrade -y --best --allowerasing --refresh 

# Eventually removes unused packages
sudo dnf distro-sync -y

